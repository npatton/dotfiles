#
# ~/.bashrc
#

alias ipaddr="curl ifconfig.me ; echo"
alias pacu="yes | sudo pacman -Syyu"
alias paci="yes | sudo pacman -S"
alias pacr="yes | sudo pacman -Rs"
alias paco="sudo pacman -Rns $(pacman -Qtdg)"
alias ls="ls --color"
alias srot="xrandr --output LVDS1 --rotate"
alias kali="sh ~/Scripts/vm_control"
alias open="xdg-open"
eval "$(thefuck --alias)"

[[ -f ~/.bash_profile ]] && . ~/.bash_profile
